from app import create_app
import pytest

@pytest.fixture
def client():
    app = create_app()
    client = app.test_client()
    yield client

def test_index(client):
    response = client.get("/")
    assert response.status_code == 200