FROM python:3.11.4-slim-bullseye

WORKDIR /app

COPY app /app/app
COPY requirements.txt /app/requirements.txt
COPY wsgi.py /app/wsgi.py

RUN pip install -r requirements.txt

EXPOSE 80
ENTRYPOINT ["gunicorn", "--workers=2", "--threads=4", "-b 0.0.0.0:80", "wsgi:app"]